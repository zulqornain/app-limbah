-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2020 at 08:51 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbp_limbah`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` varchar(10) NOT NULL DEFAULT '',
  `nm_barang` varchar(40) DEFAULT NULL,
  `stok` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nm_barang`, `stok`) VALUES
('BR0001', 'Kursi Plastik', 19),
('BR0002', 'asa', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_angkut`
--

CREATE TABLE `data_angkut` (
  `Id` int(11) NOT NULL,
  `tgl_angkut` date DEFAULT NULL,
  `kd_limbah` varchar(10) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_angkut`
--

INSERT INTO `data_angkut` (`Id`, `tgl_angkut`, `kd_limbah`, `jumlah`) VALUES
(10, '2020-03-06', 'KL0001', 1),
(18, '2020-03-16', '', 10),
(19, '2020-03-16', 'KL0002', 10),
(20, '2020-03-16', 'KL0002', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_limbah`
--

CREATE TABLE `data_limbah` (
  `id` int(11) NOT NULL,
  `kd_limbah` varchar(10) NOT NULL,
  `namalimbah` varchar(20) NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `tgl_penambahan` date DEFAULT NULL,
  `harga` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_limbah`
--

INSERT INTO `data_limbah` (`id`, `kd_limbah`, `namalimbah`, `stok`, `tgl_penambahan`, `harga`) VALUES
(1, 'KL0001', 'kardus', 100, '2020-03-05', '5000'),
(4, 'KL0002', 'botol Aqua', 10, '2020-03-16', '2500');

-- --------------------------------------------------------

--
-- Table structure for table `kirim_barang`
--

CREATE TABLE `kirim_barang` (
  `id_kirim` char(10) NOT NULL DEFAULT '',
  `kd_toko` char(10) DEFAULT NULL,
  `id_barang` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `jumlah` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kirim_barang`
--

INSERT INTO `kirim_barang` (`id_kirim`, `kd_toko`, `id_barang`, `tanggal`, `jumlah`) VALUES
('KRM0001', 'KT0001', 'BR0001', '2020-03-02', '3'),
('KRM0002', 'KT0001', 'BR0001', '2020-03-02', '2');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(8) NOT NULL,
  `level` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `level`) VALUES
(1, 'admin', 'admin', 'admin'),
(4, 'gudang', 'gudang', 'gudang'),
(6, 'kurir', 'kurir', 'kurir'),
(7, 'produksi', 'produksi', 'produksi');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `kd_pegawai` varchar(10) NOT NULL,
  `namapegawai` varchar(20) NOT NULL,
  `tempatlahir` varchar(20) NOT NULL,
  `tgllahir` date NOT NULL,
  `jeniskelamin` varchar(10) NOT NULL,
  `alamat` varchar(70) NOT NULL,
  `notelepon` varchar(12) NOT NULL,
  `email` varchar(25) NOT NULL,
  `statuspegawai` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `kd_pegawai`, `namapegawai`, `tempatlahir`, `tgllahir`, `jeniskelamin`, `alamat`, `notelepon`, `email`, `statuspegawai`) VALUES
(6, 'KP0001', 'bashori', 'tangerang', '2020-03-01', 'Laki - Lak', 'bumi indah psar kemis', '08263672882', 'bashori@gmail.com', 'limbah');

-- --------------------------------------------------------

--
-- Table structure for table `proses_daur`
--

CREATE TABLE `proses_daur` (
  `Id` int(11) NOT NULL,
  `tgl_pelaksanaan` datetime DEFAULT NULL,
  `jml_buat` int(4) DEFAULT NULL,
  `id_ref` varchar(10) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1 proses, 2 selesai, 3 berhasil, 4 gagal'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proses_daur`
--

INSERT INTO `proses_daur` (`Id`, `tgl_pelaksanaan`, `jml_buat`, `id_ref`, `status`) VALUES
(1, '2020-03-12 00:00:00', 1, 'BR0001', 2),
(2, '2020-03-19 00:00:00', 10, 'BR0001', 2),
(3, '2020-03-17 00:00:00', 2, 'BR0001', 2),
(4, '2020-03-19 00:00:00', 3, 'BR0001', 2),
(5, '2020-03-14 00:00:00', 2, 'BR0001', 2),
(6, '2020-03-19 00:00:00', 1, 'BR0001', 2),
(7, '2020-03-16 00:00:00', 1, 'BR0002', 2),
(8, '2020-03-16 00:00:00', 1, 'BR0002', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_barang`
--

CREATE TABLE `ref_barang` (
  `id_ref` varchar(10) NOT NULL DEFAULT '',
  `ref_nama` varchar(50) DEFAULT NULL,
  `waktu_pengerjaan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_barang`
--

INSERT INTO `ref_barang` (`id_ref`, `ref_nama`, `waktu_pengerjaan`) VALUES
('BR0001', 'Kursi Plastik', 12),
('BR0002', 'asa', 12);

-- --------------------------------------------------------

--
-- Table structure for table `ref_barang_det`
--

CREATE TABLE `ref_barang_det` (
  `id_ref_det` int(11) NOT NULL,
  `kd_limbah` varchar(10) DEFAULT NULL,
  `ambil_stok` int(11) DEFAULT NULL,
  `id_ref` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_barang_det`
--

INSERT INTO `ref_barang_det` (`id_ref_det`, `kd_limbah`, `ambil_stok`, `id_ref`) VALUES
(11, 'KL0002', 2, 'BR0001'),
(12, 'KL0001', 1, 'BR0001'),
(13, 'KL0002', 1, 'BR0002');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `kd_supplier` varchar(10) NOT NULL,
  `nama_tempat` varchar(70) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `kd_supplier`, `nama_tempat`, `alamat`, `telp`, `email`) VALUES
(3, 'SP0001', 'pangkalan 1', 'bumi indah psar kemis', '081235627182', 'pangkalan1@gmail.com'),
(4, 'SP0002', 'pangkalan 2', 'sepatan timur', '08211231231', 'pangkalan2@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `toko`
--

CREATE TABLE `toko` (
  `id` int(11) NOT NULL,
  `kd_toko` varchar(10) NOT NULL,
  `namatempat` varchar(50) NOT NULL,
  `alamat` varchar(70) NOT NULL,
  `notelepon` varchar(12) NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toko`
--

INSERT INTO `toko` (`id`, `kd_toko`, `namatempat`, `alamat`, `notelepon`, `email`) VALUES
(3, 'KT0001', 'toko serba ada', 'bumi indah psar kemis', '08263672882', 'kura24795@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `data_angkut`
--
ALTER TABLE `data_angkut`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `data_limbah`
--
ALTER TABLE `data_limbah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kirim_barang`
--
ALTER TABLE `kirim_barang`
  ADD PRIMARY KEY (`id_kirim`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proses_daur`
--
ALTER TABLE `proses_daur`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `ref_barang`
--
ALTER TABLE `ref_barang`
  ADD PRIMARY KEY (`id_ref`);

--
-- Indexes for table `ref_barang_det`
--
ALTER TABLE `ref_barang_det`
  ADD PRIMARY KEY (`id_ref_det`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toko`
--
ALTER TABLE `toko`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_angkut`
--
ALTER TABLE `data_angkut`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `data_limbah`
--
ALTER TABLE `data_limbah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `proses_daur`
--
ALTER TABLE `proses_daur`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ref_barang_det`
--
ALTER TABLE `ref_barang_det`
  MODIFY `id_ref_det` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `toko`
--
ALTER TABLE `toko`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
