<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <a href="?pengelolaanlimbah=barang_jadi"><button type="button" class="hide btn btn-primary waves-effect waves-light"> <b>Cek Barang</b></button></a>
                        </div>
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <th width="20"><center>Kode Kirim</center></th>
                                    <th><center>Nama Toko</center></th>
                                    <th><center>Nama Barang</center></th>
                                    <th><center>Tanggal Kirim</center></th>
                                    <th><center>Jumlah</center></th>
                                </thead>
                                <tbody>
                                	<?php $query = "SELECT * FROM kirim_barang kb JOIN toko t USING(kd_toko) Join barang b USING (id_barang) ORDER BY kb.tanggal DESC" ?>
                                <?php foreach(mysqli_query($conn, $query) AS $no => $ref){ ?>
                                    <tr>
                                        <td align="center"><?= $ref['id_kirim'] ?></td>
                                        <td align="center"><?= $ref['namatempat'] ?></td>
                                        <td align="center"><?= $ref['nm_barang'] ?></td>
                                        <td><?= $ref['tanggal'] ?></td>
                                        <td align="center"><?= $ref['jumlah'] ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>