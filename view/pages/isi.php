<?php

	if(isset($_GET['pengelolaanlimbah'])){
		$page = $_GET['pengelolaanlimbah'];

		switch ($page) {
			case 'supplier':
				include "./view/supplier.php";
				break;
			case 'form_supplier':
				include "./view/form_supplier.php";
				break;
			case 'edit_supplier':
				include "./view/edit_supplier.php";
				break;
			case 'hapus_supplier':
				include "./view/hapus_supplier.php";
				break;

			case 'pegawai_limbah':
				include "./view/pegawai_limbah.php";
				break;
			case 'form_pegawai_limbah':
				include "./view/form_pegawai_limbah.php";
				break;
			case 'edit_pegawai_limbah':
				include "./view/edit_pegawai_limbah.php";
				break;
			case 'hapus_pegawai_limbah':
				include "./view/hapus_pegawai_limbah.php";
				break;

			case 'data_limbah':
				include "./view/data_limbah.php";
				break;	
			case 'form_data_limbah':
				include "./view/form_data_limbah.php";
				break;
			case 'edit_data_limbah':
				include "./view/edit_data_limbah.php";
				break;
			case 'hapus_data_limbah':
				include "./view/hapus_data_limbah.php";
				break;

			case 'data_angkut':
				include "./view/data_angkut.php";
				break;	
			case 'form_data_angkut':
				include "./view/form_data_angkut.php";
				break;
			case 'edit_data_angkut':
				include "./view/edit_data_angkut.php";
				break;
			case 'hapus_data_angkut':
				include "./view/hapus_data_angkut.php";
				break;
		
			case 'toko':
				include "./view/toko.php";
				break;		
			case 'form_toko':
				include "./view/form_toko.php";
				break;
			case 'edit_toko':
				include "./view/edit_toko.php";
				break;
			case 'hapus_toko':
				include "./view/hapus_toko.php";
				break;

			//TRANSAKSI
			case 'referensibarang':
				include "./view/referensi_barang.php";
				break;

			case 'prsreferensi':
				include "./view/proses/proses_ref.php";
				break;

			case 'prosesdaur':
				include "./view/proses/proses_daur.php";
				break;

			case 'daurulang':
				include "./view/daur_ulang.php";
				break;

			case 'kirim_barang':
				include "./view/kirim_barang.php";
				break;
			case 'barang_jadi':
				include "./view/barang_jadi.php";
				break;
			case 'form_kirim_barang':
				include "./view/form_kirim_barang.php";
				break;

			case 'laporan_daur':
				include "./view/laporan/laporan_daur.php";
				break;

			case 'laporan_supplier':
				include "./view/laporan/laporan_supplier.php";
				break;
			case 'laporan_toko':
				include "./view/laporan/laporan_toko.php";
				break;
			case 'laporan_data_limbah':
				include "./view/laporan/laporan_data_limbah.php";
				break;
			case 'laporan_data_angkut':
				include "./view/laporan/laporan_data_angkut.php";
				break;

			case 'logout':
				include "./view/logout.php";
				break;
				
			default:
				include "./view/pages/404.php";
				break;
		}
	}else{
		include "dashboard.php";
	}

	?>