<div class="vertical-menu">

<div data-simplebar class="h-100">

    <!--- Sidemenu -->
    <div id="sidebar-menu">
        <!-- Left Menu Start -->
        <ul class="metismenu list-unstyled" id="side-menu">
            <li class="menu-title">Main</li>

            <li>
                <a href="index.php" class="waves-effect">
                    <i class="ti-home"></i><span class="badge badge-pill badge-primary float-right">2</span>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="menu-title">DATA MASTER</li>
            <li>
                <a href="javascript: void(0);" class="has-arrow waves-effect">
                    <i class="mdi mdi-account-edit-outline"></i>
                    <span>User</span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="?pengelolaanlimbah=supplier">Suplier</a></li>
                    <li><a href="?pengelolaanlimbah=pegawai_limbah">Pegawai Limbah</a></li>
                </ul>
            </li>

            <li>
                <a href="javascript: void(0);" class="has-arrow waves-effect">
                    <i class="mdi mdi-account-edit-outline"></i>
                    <span>Input Limbah</span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="?pengelolaanlimbah=data_limbah">Data Limbah</a></li>
                    <li><a href="?pengelolaanlimbah=data_angkut">Data Angkut</a></li>
                </ul>
            </li>

            <li>
                <a href="?pengelolaanlimbah=toko" class="waves-effect">
                    <i class="ti-home"></i>
                    <span>Toko</span>
                </a>
            </li>
            <li class="menu-title">TRANSAKSI</li>
            <li >
                <a href="?pengelolaanlimbah=referensibarang" class="waves-effect">
                    <i class="ti-home"></i>
                    <span>Referensi Barang</span>
                </a>
            </li>
            <li>
                <a href="?pengelolaanlimbah=daurulang" class="waves-effect">
                    <i class="ti-home"></i>
                    <span>Daur Ulang</span>
                </a>
            </li>
            <li>
                <a href="?pengelolaanlimbah=kirim_barang" class="waves-effect">
                    <i class="ti-home"></i>
                    <span>Kirim Barang</span>
                </a>
            </li>
            <li class="menu-title">LAPORAN</li>
            <li>
                <a href="javascript: void(0);" class="has-arrow waves-effect">
                    <i class="mdi mdi-account-edit-outline"></i>
                    <span>Cetak Laporan</span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="?pengelolaanlimbah=laporan_supplier">Laporan Supplier</a></li>
                    <li><a href="?pengelolaanlimbah=laporan_toko">Laporan Toko</a></li>
                    <li><a href="?pengelolaanlimbah=laporan_data_limbah">Laporan Data Limbah</a></li>
                    <li><a href="?pengelolaanlimbah=laporan_data_angkut">Laporan Data Angkut</a></li>
                    <!-- <li class="menu-title">Proses Daur Ulang</li>     -->
                    <li><a href="?pengelolaanlimbah=laporan_daur">Laporan Proses Daur</a></li>
                </ul>
            </li>
            <li class="menu-title">LAINNYA</li>
            <li>
                <a href="logout.php" class="waves-effect">
                    <i class="ti-home"></i>
                    <span>LOG OUT</span>
                </a>
            </li>

           



        </ul>
    </div>
    <!-- Sidebar -->
</div>
</div>