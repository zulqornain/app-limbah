<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <button type="button" class="hide btn btn-primary waves-effect waves-light" onclick="window.print()"><i class="fa fa-print"></i> <b>Cetak</b></button>
                        </div>
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <th width="20"><center>#</center></th>
                                    <th><center>Tgl Proses</center></th>
                                    <th><center>Nama Barang</center></th>
                                    <th><center>Jumlah Barang</center></th>
                                    <th><center>Pengerjaan</center></th>
                                </thead>
                                <tbody>
                                	<?php $query = "SELECT * FROM proses_daur a JOIN ref_barang b USING(id_ref) WHERE a.status > 1 ORDER BY a.tgl_pelaksanaan DESC" ?>
                                <?php foreach(mysqli_query($conn, $query) AS $no => $ref){ ?>
                                    <tr>
                                        <td align="center"><?= $no+1 ?></td>
                                        <td align="center"><?= $ref['tgl_pelaksanaan'] ?></td>
                                        <td><?= $ref['ref_nama'] ?></td>
                                        <td align="center"><?= $ref['jml_buat'] ?> pcs</td>
                                        <td align="center">&plusmn; <?= $ref['waktu_pengerjaan'] ?> Jam</td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>