<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <button type="button" class="hide btn btn-primary waves-effect waves-light" onclick="window.print()"><i class="fa fa-print"></i> <b>Cetak</b></button>
                        </div>
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <th width="20"><center>Kode Limbah</center></th>
                                    <th><center>Nama Limbah</center></th>
                                    <th><center>Stok</center></th>
                                    <th><center>Tanggal Penambahan</center></th>
                                    <th><center>Harga</center></th>

                                </thead>
                                <tbody>
                                	<?php $query = "SELECT * FROM data_limbah" ?>
                                <?php foreach(mysqli_query($conn, $query) AS $no => $row){ ?>
                                    <tr>
                                    <td><?php echo $row['kd_limbah']; ?></td>
                                    <td><?php echo $row['namalimbah']; ?></td>
                                    <td><?php echo $row['stok']; ?></td>
                                    <td><?php echo $row['tgl_penambahan']; ?></td>
                                    <td><?php echo $row['harga']; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>