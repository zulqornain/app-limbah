<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <button type="button" class="hide btn btn-primary waves-effect waves-light" onclick="window.print()"><i class="fa fa-print"></i> <b>Cetak</b></button>
                        </div>
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <th width="20"><center>Kode Supplier</center></th>
                                    <th><center>Nama Tempat</center></th>
                                    <th><center>Alamat</center></th>
                                    <th><center>No Telpon</center></th>
                                    <th><center>Email</center></th>

                                </thead>
                                <tbody>
                                	<?php $query = "SELECT * FROM supplier" ?>
                                <?php foreach(mysqli_query($conn, $query) AS $no => $row){ ?>
                                    <tr>
                                    <td><?php echo $row['kd_supplier']; ?></td>
                                    <td><?php echo $row['nama_tempat']; ?></td>
                                    <td><?php echo $row['alamat']; ?></td>
                                    <td><?php echo $row['telp']; ?></td>
                                    <td><?php echo $row['email']; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>