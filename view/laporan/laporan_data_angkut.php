<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <button type="button" class="hide btn btn-primary waves-effect waves-light" onclick="window.print()"><i class="fa fa-print"></i> <b>Cetak</b></button>
                        </div>
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <th width="20"><center>Kode Angkut</center></th>
                                    <th><center>Nama Limbah</center></th>
                                    <th><center>Tanggal Angkut</center></th>
                                    <th><center>Jumlah Barang</center></th>
                                </thead>
                                <tbody>
                                	<?php $query = "SELECT * FROM data_angkut a JOIN data_limbah b USING(kd_limbah) WHERE a.jumlah > 1 ORDER BY a.tgl_angkut DESC" ?>
                                <?php foreach(mysqli_query($conn, $query) AS $no => $ref){ ?>
                                    <tr>
                                        <td align="center"><?= $ref['kd_limbah'] ?></td>
                                        <td align="center"><?= $ref['namalimbah'] ?></td>
                                        <td><?= $ref['tgl_angkut'] ?></td>
                                        <td align="center"><?= $ref['jumlah'] ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>