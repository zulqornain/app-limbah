<?php

include"./config/koneksi.php";
// mencari kode barang dengan nilai paling besar
$query = "SELECT max(kd_pegawai) as maxKode FROM pegawai";
$hasil = mysqli_query($conn,$query);
$data = mysqli_fetch_array($hasil);
$kd_pegawai_tampil = $data['maxKode'];

// mengambil angka atau bilangan dalam kode anggota terbesar,
// dengan cara mengambil substring mulai dari karakter ke-1 diambil 6 karakter
// misal 'BRG001', akan diambil '001'
// setelah substring bilangan diambil lantas dicasting menjadi integer
$noUrut = (int) substr($kd_pegawai_tampil, 2, 4);

// bilangan yang diambil ini ditambah 1 untuk menentukan nomor urut berikutnya
$noUrut++;

// membentuk kode anggota baru
// perintah sprintf("%03s", $noUrut); digunakan untuk memformat string sebanyak 3 karakter
// misal sprintf("%03s", 12); maka akan dihasilkan '012'
// atau misal sprintf("%03s", 1); maka akan dihasilkan string '001'
$char = "KP";
$kd_pegawai_tampil = $char . sprintf("%04s", $noUrut);
{
?>
<div class="main-content">

                <div class="page-content">
                  <div class="container-fluid">

                  <!-- ini kontent -->
     <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Pegawai Limbah</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Form Pegawai Limbah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <form action="" method="POST">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Form Pegawai Limbah</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Kode Pegawai</label>
                  <input type="text" readonly value="<?php echo $kd_pegawai_tampil;?>" name="kd_pegawai" id="kd_pegawai" class="form-control">
<?php }?>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Nama Pegawai</label>
                  <input type="text" name="namapegawai" id="namapegawai" class="form-control">
                </div>
                <div class="form-group">
                  <label>Tempat Lahir</label>
                  <input type="text" name="tempatlahir" id="tempatlahir" class="form-control">
                </div>
                <div class="form-group">
                  <label>Tanggal Lahir</label>
                  <input type="date" name="tgllahir" id="tgllahir" class="form-control">
                </div>
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select class="select2 form-control" name="jeniskelamin" data-placeholder="Select a State" style="width: 100%;">
                    <option value="Laki - Laki">Laki - Laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" name="alamat" id="alamat" class="form-control">
                </div>
                <div class="form-group">
                  <label>Nomer Telpon</label>
                  <input type="text" name="notelepon" id="notelepon" class="form-control">
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="text" name="email" id="email" class="form-control">
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              
              </div>
              
              <div class="card-footer">
                <input type="submit" name="save" value="save" class="btn btn-block btn-primary">
              </div>
    </section>
    </form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
                  <!-- tutup content -->


                  </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


                
               <!-- ini footer -->
            
            <!-- end main content-->

</div>

 
 
 

<?php
include"./config/koneksi.php";
if(isset($_POST['save'])){
  $kd_pegawai=$_POST['kd_pegawai'];
  $namapegawai=$_POST['namapegawai'];
  $tempatlahir=$_POST['tempatlahir'];
  $tgllahir=$_POST['tgllahir'];
  $jeniskelamin=$_POST['jeniskelamin'];
  $alamat=$_POST['alamat'];
  $notelepon=$_POST['notelepon'];
  $email=$_POST['email'];

  mysqli_query($conn, "INSERT INTO pegawai(kd_pegawai,namapegawai,tempatlahir,tgllahir,jeniskelamin,alamat,notelepon,email,statuspegawai) VALUES('$kd_pegawai','$namapegawai','$tempatlahir','$tgllahir','$jeniskelamin','$alamat','$notelepon','$email','limbah')");
  echo"<script>alert ('Data sudah masuk ke database')</script>";
  echo"<meta http-equiv='refresh' content=1;URL=?pengelolaanlimbah=pegawai_limbah>";
  }
  ?>