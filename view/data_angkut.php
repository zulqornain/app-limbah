<div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                  <!-- ini kontent -->
                    <section class="content-header">
                      <div class="container-fluid">
                        <div class="row mb-2">
                          <div class="col-sm-6">
                            <h1>Data Angkut</h1>
                          </div>
                          <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                              <li class="breadcrumb-item"><a href="#">Home</a></li>
                              <li class="breadcrumb-item active">Data Angkut</li>
                            </ol>
                          </div>
                        </div>
                      </div><!-- /.container-fluid -->
                    </section>

                    <!-- Main content -->
                    <section class="content">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <h3 class="card-title"><a href="?pengelolaanlimbah=form_data_angkut"><button type="button" class="btn btn-block btn-primary btn-lg">Add Data Angkut</button></a></h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                              <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Tanggal Angkut</th>
                                  <th>Kode Limbah</th>
                                  <th>Nama Limbah</th>
                                  <th>Jumlah Limbah</th>
                                  <th>ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                  <?php $nomor=1;
                                  $cek = mysqli_query($conn, "SELECT * FROM data_limbah dl inner join data_angkut da on dl.kd_limbah=da.kd_limbah");?>
                                  <?php while ($row =mysqli_fetch_array($cek)) { ?>

                                  <tr>
                                    <td><?php echo $nomor++; ?></td>
                                    <td><?php echo $row['tgl_angkut']; ?></td>
                                    <td><?php echo $row['kd_limbah']; ?></td>
                                    <td><?php echo $row['namalimbah']; ?></td>
                                    <td><?php echo $row['jumlah']; ?></td>
                                    <td>
                                      <a href="?pengelolaanlimbah=hapus_data_angkut&no=<?php echo $row['kd_limbah'];?>"><button class="btn-danger btn">Hapus</button></a>

                                      <a href="?pengelolaanlimbah=edit_data_angkut&no=<?php echo $row['kd_limbah'];?>"><button class="btn-warning btn">Ubah</button></a>
                                    </td>
                                  </tr>
                    
                  <?php } ?>
                                  </tbody>
                              </table>
                            </div>
                            <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </section>
                  <!-- tutup content -->


                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


                
               <!-- ini footer -->
            </div>
            <!-- end main content-->

        </div>

