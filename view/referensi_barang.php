<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> <b>Ref. Barang</b></button>
                        </div>
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <th><center>#</center></th>
                                    <th><center>Referensi Nama Barang</center></th>
                                    <th><center>Waktu Pengerjaan</center></th>
                                    <th><center>Bahan - Bahan</center></th>
                                    <th><center>Aksi</center></th>
                                </thead>
                                <tbody>
                                <?php foreach(mysqli_query($conn, "SELECT * FROM ref_barang ORDER BY ref_nama ASC") AS $no => $ref){ ?>
                                    <tr>
                                        <td><?= $no+1 ?></td>
                                        <td><?= $ref['ref_nama'] ?></td>
                                        <td>Kurang dari <?= $ref['waktu_pengerjaan'] ?> Jam</td>
                                        <td>
                                            <table class="table">
                                                <?php foreach (mysqli_query($conn, "SELECT * FROM ref_barang_det a JOIN data_limbah b USING(kd_limbah) WHERE a.id_ref = '$ref[id_ref]'") as $val) { ?>
                                                    <tr>
                                                        <td><?= $val['namalimbah'] ?></td>
                                                        <td><?= $val['ambil_stok'] ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </table>
                                        </td>
                                        <td align="center">
                                            <script type="text/javascript">
                                                function errProses(param){
                                                    return null;
                                                }
                                            </script>
                                            <?php foreach (mysqli_query($conn, "SELECT * FROM ref_barang_det a JOIN data_limbah b USING(kd_limbah) WHERE a.id_ref = '$ref[id_ref]'") as $val) { 
                                                if($val['ambil_stok'] > $val['stok']){ ?>
                                                
                                                    <script type="text/javascript">
                                                        function errProses(param){
                                                            return alert('Terdapat stok yang tidak mencukupi, Silahkan periksa kembali bahan limbah anda.');
                                                        }
                                                    </script>

                                                <?php }?>
                                            <?php } ?>
                                                <button type="button" data-toggle="modal" data-target=".proses" class="btn btn-info" onclick="errProses('<?= $ref['id_ref'] ?>')"><b>Proses</b></button>   
                                            <button class="btn btn-danger"><b>Hapus</b></button>
                                        </td>
                                    </tr>
                                        <div class="modal fade proses" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title mt-0" id="myLargeModalLabel">Proses Daur Ulang</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="?pengelolaanlimbah=prosesdaur&data=<?= $ref['id_ref'] ?>" method="POST">
                                                            <div class="form-group row">
                                                                <label for="example-search-input" class="col-sm-4 col-form-label">Pengerjaan</label>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control" name="wp" type="date" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="example-search-input" class="col-sm-4 col-form-label">Jumlah Pembuatan</label>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control" name="jb" type="number" >
                                                                </div>
                                                            </div>
                                                            <div class="card-footer">
                                                                <button class="btn btn-primary"><b>PROSES</b></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Buat Daftar Referensi Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="?pengelolaanlimbah=prsreferensi&act=add" method="POST">
                    <div class="form-group row">
                        <label for="example-search-input" class="col-sm-2 col-form-label">Nama Barang</label>
                        <div class="col-sm-10">
                            <input class="form-control" name="nm_barang" type="text" placeholder="Masukan nama barang" id="example-text-input">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="time" class="col-sm-2 col-form-label">Pembuatan / 1 Pcs</label>
                        <div class="col-sm-10"  >
                            <select class="form-control" id="time"name="time" required="">
                                <option value="" hidden="">--Pilih Waktu--</option>
                                <option value="6">Kurang dari 6 Jam</option>
                                <option value="12">Kurang dari 12 Jam</option>
                                <option value="24">Kurang dari 24 Jam</option>
                                <option value="36">Kurang dari 36 Jam</option>
                                <option value="48">Kurang dari 48 Jam</option>
                                <option value="60">Kurang dari 60 Jam</option>
                                <option value="100">Kurang dari 100 Jam</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-search-input" class="col-sm-2 col-form-label">Jumlah Bahan</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="number" placeholder="Jumlah Bahan" id="jml" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 text-right">
                            <button type="button" class="btn btn-info" id="proses"><i class="fa fa-proccess"></i> <b>Proses</b></button>
                        </div>
                    </div>

                    <div id="lop" style="display: none;">
                        <hr>
                        <div id="bahan"></div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    function tag(jml){
        var html = [];
        no=0;
        for (var i = 0; i < jml; i++) {
            no++;
            html[i] =
            '<div class="form-group row">'+
                '<label for="prs_'+no+'" class="col-sm-2 col-form-label">Bahan '+no+'</label>'+
                '<div class="col-sm-10">'+
                    '<select id="prs_'+no+'" class="form-control" name="prs[]" required="">'+
                        '<option valie="" hidden="">--Pilih Bahan--</option>'+
                        <?php 
                            foreach (mysqli_query($conn, "SELECT * FROM data_limbah ORDER BY namalimbah ASC") AS $val) { 
                        ?>
                        '<option value="<?= $val['kd_limbah'] ?>"><?= $val['namalimbah'] ?></option>'+
                        <?php } ?>
                    '</select>'+
                '</div>'+
            '</div>'+
            '<div class="form-group row">'+
                '<label for="stok_'+no+'" class="col-sm-2 col-form-label">Jumlah Limbah</label>'+
                '<div class="col-sm-10">'+
                    '<input type="number" id="stok_'+no+'" class="form-control" name="stok[]" required="">'+
                '</div>'+
            '</div>';
        }
        html[jml] = 
        '<div class="modal-footer text-right">'+
            '<button type="submit" class="btn btn-primary">SIMPAN</button>'+
        '</div>';

        return html;
    }

    var proses  = document.getElementById('proses');
    var lop     = document.getElementById('lop');
    var bahan   = document.getElementById('bahan');

    proses.addEventListener('click', function(){
        var jml = document.getElementById('jml');
        if(jml.value <= 0 ){
            alert('Form jumlah bahan tidak boleh kosong atau 0.');
        };
        console.log(tag(jml.value));

        lop.style.display = 'block';
        bahan.innerHTML= tag(jml.value);
    })

</script>